from pydantic import BaseModel


class SchemaUserIn(BaseModel):
    nom: str
    prenoms: str
    email: str
    is_active: bool | None

    class Config:
        orm_mode = True


class SchemaUserOut(BaseModel):
    id: int
    nom: str
    prenoms: str
    email: str
    is_active: bool | None

    class Config:
        orm_mode = True
