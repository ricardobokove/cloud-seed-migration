from typing import List
from students.models import students
from students.schemas import SchemaUserIn, SchemaUserOut
from students.database import db
from fastapi import FastAPI, status

app = FastAPI()


@app.on_event("startup")
async def startup():
    await db.connect()


@app.on_event("shutdown")
async def shutdown():
    await db.disconnect()


@app.get("/students/", response_model=List[SchemaUserOut])
async def all_user():
    query = students.select()
    users = await db.fetch_all(query)
    return users


@app.post("/students/", response_model=SchemaUserOut, status_code=status.HTTP_201_CREATED)
async def create_use(student: SchemaUserIn):
    query = students.insert().values(nom=student.nom,
                                     prenoms=student.prenoms,
                                     email=student.email,
                                     is_active=student.is_active)
    last_record_uuid = await db.execute(query)
    query = students.select().where(students.c.id == last_record_uuid)
    result = await db.fetch_one(query)
    return {**result}

