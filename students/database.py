import os
from pathlib import Path

from databases import Database
from dotenv import load_dotenv
import sqlalchemy

BASE_DIR = Path(__file__).resolve().parent.parent
load_dotenv(os.path.join(BASE_DIR, ".env"))

db = Database(os.environ["DATABASE_URL"])
metadata = sqlalchemy.MetaData()
