from sqlalchemy import Boolean, Column, Table, String, Integer
from students.database import metadata

students = Table(
    "students",
    metadata,
    Column("id", Integer, primary_key=True),
    Column("nom", String(30), unique=False),
    Column("prenoms", String(100), unique=False),
    Column("email", String, unique=True, index=True),
    Column("is_active", Boolean, default=True)
)
